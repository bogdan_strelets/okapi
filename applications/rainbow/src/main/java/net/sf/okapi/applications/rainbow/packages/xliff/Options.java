/*===========================================================================
  Copyright (C) 2008-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.applications.rainbow.packages.xliff;

import net.sf.okapi.common.StringParameters;

public class Options extends StringParameters{

	private static final String GMODE = "gMode";
	private static final String INCLUDENOTRANSLATE = "includeNoTranslate";
	private static final String SETAPPROVEDASNOTRANSLATE = "setApprovedAsNoTranslate";
	private static final String MESSAGE = "message";
	private static final String COPYSOURCE = "copySource";
	private static final String INCLUDEALTTRANS = "includeAltTrans";
	
	public boolean getIncludeNoTranslate () {
		return getBoolean(INCLUDENOTRANSLATE);
	}

	public void setIncludeNoTranslate (boolean includeNoTranslate) {
		setBoolean(INCLUDENOTRANSLATE, includeNoTranslate);
	}
	
	public boolean getSetApprovedAsNoTranslate () {
		return getBoolean(SETAPPROVEDASNOTRANSLATE);
	}

	public void setSetApprovedAsNoTranslate (boolean setApprovedAsNoTranslate) {
		setBoolean(SETAPPROVEDASNOTRANSLATE, setApprovedAsNoTranslate);
	}

	public boolean getCopySource () {
		return getBoolean(COPYSOURCE);
	}
	
	public void setCopySource (boolean copySource) {
		setBoolean(COPYSOURCE, copySource);
	}

	
	public boolean getGMode() {
		return getBoolean(GMODE);
	}

	public void setGMode(boolean gMode) {
		setBoolean(GMODE, gMode);
	}

	public String getMessage() {
		return getString(MESSAGE);
	}

	public void setMessage(String message) {
		setString(MESSAGE, message);
	}
	
	public boolean getIncludeAltTrans () {
		return getBoolean(INCLUDEALTTRANS);
	}

	public void setIncludeAltTrans (boolean includeAltTrans) {
		setBoolean(INCLUDEALTTRANS, includeAltTrans);
	}

	public Options () {
		super();
	}
	
	public void reset() {
		super.reset();
		setGMode(false);
		setIncludeNoTranslate(true);
		setSetApprovedAsNoTranslate(false);
		setMessage("");
		setCopySource(true);
		setIncludeAltTrans(true);
	}

	public void fromString (String data) {
		super.fromString(data);
		
		// Make sure the we can merge later
		if ( !getIncludeNoTranslate()) {
			setSetApprovedAsNoTranslate(false);
		}
	}
}
