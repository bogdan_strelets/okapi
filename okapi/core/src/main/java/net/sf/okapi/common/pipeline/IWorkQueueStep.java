package net.sf.okapi.common.pipeline;

import java.util.LinkedList;

/**
 * Step that can process multiple events concurrently.
 * @param <T> - return type of {@link ICallableStep}, i.e. Event, SortableEvent
 */
public interface IWorkQueueStep<T> extends IPipelineStep {

	/**
	 * Gets the main step wrapped by IWorkQueueStep
	 * @return the main step used as template by {@link ICallableStep} steps
	 */
	public IPipelineStep getMainStep();	
	
	/**
	 * Gets the list of {@link ICallableStep} based on the main or template step.
	 * @return the {@link ICallableStep} steps
	 */
	public LinkedList<ICallableStep<T>> getCallableSteps();
	
	/** 
	 * Get the number of work queues defined for this step
	 * @return the number of work queue
	 */
	public int getWorkQueueCount();
	
	/**
	 * Used when the empty constructor is called
	 */
	public void init() throws InstantiationException, IllegalAccessException;
	
	/**
	 * Sets the main (template for all callabale steps) pipeline step
	 * <b>MUST BE CALLED BEFORE init()</b>
	 * @param step - the main step
	 */
	public void setMainStep(IPipelineStep step);
	
	/**
	 * Set the work queue count
	 * <b>MUST BE CALLED BEFORE init()</b>
	 * @param workQueueCount - the number of step work queues
	 */
	public void setWorkQueueCount(int workQueueCount);
}
