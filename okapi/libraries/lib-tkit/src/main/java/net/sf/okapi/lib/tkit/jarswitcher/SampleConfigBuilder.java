/*===========================================================================
  Copyright (C) 2008-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.tkit.jarswitcher;

import net.sf.okapi.common.Util;

/**
 * A snippet, demonstrating creation of a version manager configuration file.
 */
public class SampleConfigBuilder {

	public static void main(String[] args) {
		VersionManager manager = new VersionManager();
		
		// Reset the default config from resources
		manager.getVersions().clear();
	
		// Create versions. The versions will be auto-sorted by date.
		manager.add("m23", "/D:/git_local_repo/okapi_master/m23/okapi", "2013-9-27");
		manager.add("m19", "/D:/git_local_repo/okapi_master/m19/okapi", "2012-11-24");
		manager.add("m20", "/D:/git_local_repo/okapi_master/m20/okapi", "2013-2-17");
		manager.add("m24", "/D:/git_local_repo/okapi_master/m24/okapi", "2014-1-6");
		manager.add("m21", "/D:/git_local_repo/okapi_master/m21/okapi", "2013-4-16");
		manager.add("m22", "/D:/git_local_repo/okapi_master/m22-hotfix/okapi", "2013-7-19");
		
		// Store the configuration file at some location. Extension can be different from .json
		manager.store(Util.buildPath(System.getProperty("user.home"), "okapi_releases.json"));
	}
}
