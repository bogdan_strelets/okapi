/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.tkit.merge;

import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import net.sf.okapi.common.StringParameters;

public class Parameters extends StringParameters {
	static final String SKELETONPATH = "skeletonpath"; //$NON-NLS-1$
	static final String APPROVEDONLY = "approvedOnly"; //$NON-NLS-1$
	static final String THROW_EXCEPTION_CODE_DIFFERENCES = "throwExceptionForCodeDifferences"; //$NON-NLS-1$
	static final String THROW_EXCEPTION_SEGMENT_ID_DIFFERENCES = "throwExceptionForSegmentIdDifferences"; //$NON-NLS-1$
	static final String THROW_EXCEPTION_SEGMENT_SOURCE_DIFFERENCES = "throwExceptionForSegmentSourceDifferences"; //$NON-NLS-1$

	private InputStream skeletonInputStream;

	public Parameters () {
		super();
	}
	
	@Override
	public void reset () {
		super.reset();
		setSkeletonUri(null);
		setSkeletonInputStream(null);
		setApprovedOnly(false);
		setThrowCodeException(false);
		setThrowSegmentIdException(true);
		setThrowSegmentSourceException(false);
	}

	public URI getSkeletonUri() throws URISyntaxException {
		return new URI(getString(SKELETONPATH));
	}

	public void setSkeletonUri(URI skeletonUri) {
		String s = "";
		if (skeletonUri != null) {
			s = skeletonUri.toString();
		}
		setString(SKELETONPATH, s);
	}
	
	/**
	 * Only merge approved translations. 
	 * @return boolean - use approved target translations only?
	 */
	public boolean isApprovedOnly() {
		return getBoolean(APPROVEDONLY);
	}

	public void setApprovedOnly(boolean approvedOnly) {
		setBoolean(APPROVEDONLY, approvedOnly);
	}

	public InputStream getSkeletonInputStream() {
		return skeletonInputStream;
	}

	public void setSkeletonInputStream(InputStream skeletonInputStream) {
		this.skeletonInputStream = skeletonInputStream;
	}
	
	public boolean isThrowCodeException() {
		return getBoolean(THROW_EXCEPTION_CODE_DIFFERENCES);
	}

	public void setThrowCodeException(boolean exception) {
		setBoolean(THROW_EXCEPTION_CODE_DIFFERENCES, exception);
	}
	
	public boolean isThrowSegmentIdException() {
		return getBoolean(THROW_EXCEPTION_SEGMENT_ID_DIFFERENCES);
	}

	public void setThrowSegmentIdException(boolean exception) {
		setBoolean(THROW_EXCEPTION_SEGMENT_ID_DIFFERENCES, exception);
	}
	
	public boolean isThrowSegmentSourceException() {
		return getBoolean(THROW_EXCEPTION_SEGMENT_SOURCE_DIFFERENCES);
	}

	public void setThrowSegmentSourceException(boolean exception) {
		setBoolean(THROW_EXCEPTION_SEGMENT_SOURCE_DIFFERENCES, exception);
	}
}
