/*===========================================================================
  Copyright (C) 2008-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.xliffkit.codec;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PackageHexCodec extends BasePackageCodec {

	private static final String MASK = "_#x%04X;";
	private static final Pattern PATTERN = Pattern.compile("_#x(\\p{XDigit}+?);");
	private static final int GROUP = 1;
	
	@Override
	protected String doEncode(int codePoint) {		
		return String.format(MASK, codePoint);
	}

	@Override
	protected String doDecode(String text) {
		Matcher matcher = PATTERN.matcher(text);
	    StringBuilder buf = new StringBuilder();
	    
	    int start = 0;
	    int end = 0;
	    
	    while (matcher.find()) {
	        start = matcher.start();
	        if (start != -1) {
	        	buf.append(text.substring(end, start));
	        	int codePoint = Integer.parseInt(matcher.group(GROUP), 16);
	        	char[] replacement = Character.toChars(codePoint);
	        	buf.append(replacement);
		        end = matcher.end();
	        }
	    }
	    
	    buf.append(text.substring(end));
	    return buf.toString();
	}
}
