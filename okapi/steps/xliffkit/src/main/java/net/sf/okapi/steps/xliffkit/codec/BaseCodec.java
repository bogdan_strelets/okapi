/*===========================================================================
  Copyright (C) 2008-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.xliffkit.codec;

/**
 * Base class for text encoders/decoders. Subclasses are expected to override methods of this class
 * based on the subclass implementation logic.   
 */
public abstract class BaseCodec implements ICodec {

	@Override
	public final String encode(String text) {
		text = doEncodePreprocess(text);
		text = doEncode(text);		
		return doEncodePostprocess(text);
	}
	
	@Override
	public final String decode(String text) {
		text = doDecodePreprocess(text);
		text = doDecode(text);
		return doDecodePostprocess(text);
	}
	
	protected String doEncode(String text) {
		StringBuilder sb = new StringBuilder();
		
		final int length = text.length();
		
		for (int offset = 0; offset < length; ) {
		   final int codePoint = text.codePointAt(offset);

		   if (canEncode(codePoint)) {
			   sb.append(doEncode(codePoint));
		   }
		   else {
			   sb.append(Character.toChars(codePoint));
		   }

		   offset += Character.charCount(codePoint);
		}
		return sb.toString();
	}
	
	protected String doEncode(int codePoint) {
		return new String(Character.toChars(codePoint));
	}
	
	protected String doDecode(String text) {
		StringBuilder sb = new StringBuilder();
		
		final int length = text.length();
		
		for (int offset = 0; offset < length; ) {
		   int codePoint = text.codePointAt(offset);
		   sb.append(doDecode(codePoint));
		   
		   offset += Character.charCount(codePoint);
		}
		return sb.toString();
	}
	
	protected String doDecode(int codePoint) {
		return new String(Character.toChars(codePoint));
	}
	
	protected String doEncodePreprocess(String text) {
		return text;
	}
	
	protected String doEncodePostprocess(String text) {
		return text;
	}
	
	protected String doDecodePreprocess(String text) {
		return text;
	}
	
	protected String doDecodePostprocess(String text) {
		return text;
	}
}
