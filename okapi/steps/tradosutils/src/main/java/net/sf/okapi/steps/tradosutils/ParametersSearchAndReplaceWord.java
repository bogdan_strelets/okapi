/*===========================================================================
  Copyright (C) 2012 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
============================================================================*/

package net.sf.okapi.steps.tradosutils;

import java.util.ArrayList;

import net.sf.okapi.common.StringParameters;

public class ParametersSearchAndReplaceWord extends StringParameters {
	private static final String REGEX = "regEx";
	private static final String WHOLEWORD = "wholeWord";
	private static final String MATCHCASE = "matchCase";
	private static final String REPLACEALL = "replaceAll";
	
	public ArrayList<String[]> rules;
	
	public ParametersSearchAndReplaceWord () {
		super();
	}

	public boolean getRegEx() {
		return getBoolean(REGEX);
	}

	public void setRegEx(boolean regEx) {
		setBoolean(REGEX, regEx);
	}

	public boolean getWholeWord() {
		return getBoolean(WHOLEWORD);
	}

	public void setWholeWord(boolean wholeWord) {
		setBoolean(WHOLEWORD, wholeWord);
	}

	public boolean getMatchCase() {
		return getBoolean(MATCHCASE);
	}

	public void setMatchCase(boolean matchCase) {
		setBoolean(MATCHCASE, matchCase);
	}

	public boolean getReplaceAll() {
		return getBoolean(REPLACEALL);
	}

	public void setReplaceAll(boolean replaceALL) {
		setBoolean(REPLACEALL, replaceALL);
	}

	public void reset () {
		super.reset();
		setRegEx(false);
		setWholeWord(false);
		setMatchCase(false);
		setReplaceAll(true);
		rules = new ArrayList<String[]>();
	}

	public void addRule (String pattern[]) {
		rules.add(pattern);
	}	
	
	public ArrayList<String[]> getRules () {
		return rules;
	}	

	public void fromString (String data) {
		super.fromString(data);

		int count = buffer.getInteger("count", 0);
		for ( int i=0; i<count; i++ ) {
			String []s = new String[5];
			s[0] = buffer.getString(String.format("use%d", i), "").replace("\r", "");
			s[1] = buffer.getString(String.format("search%d", i), "").replace("\r", "");
			s[2] = buffer.getString(String.format("replace%d", i), "").replace("\r", "");
			s[3] = buffer.getString(String.format("searchFormat%d", i), "").replace("\r", "");			
			s[4] = buffer.getString(String.format("replaceFormat%d", i), "").replace("\r", "");
			rules.add(s);
		}
	}

	public String toString() {
		buffer.setInteger("count", rules.size());
		int i = 0;
		for ( String[] temp : rules ) {
			buffer.setString(String.format("use%d", i), temp[0]);
			buffer.setString(String.format("search%d", i), temp[1]);
			buffer.setString(String.format("replace%d", i), temp[2]);
			buffer.setString(String.format("searchFormat%d", i), temp[3]);
			buffer.setString(String.format("replaceFormat%d", i), temp[4]);
			i++;
		}
		return super.toString();
	}

/* SAMPLE CONFIG
#v1
regEx.b=false
wholeWord.b=false
matchCase.b=false
count.i=0
replaceALL.b=true
use0=true
search0=Hello
replace0=Bonjour
searchFormat0=Normal
replaceFormat0=Heading 1
use1=true
search1=world
replace1=cosmos
searchFormat1=Heading 1
replaceFormat1=Heading 2
*/
}
