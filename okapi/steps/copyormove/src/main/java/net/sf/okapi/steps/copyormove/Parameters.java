/*===========================================================================
 Copyright (C) 2010-2013 by the Okapi Framework contributors
 -----------------------------------------------------------------------------
 This library is free software; you can redistribute it and/or modify it 
 under the terms of the GNU Lesser General Public License as published by 
 the Free Software Foundation; either version 2.1 of the License, or (at 
 your option) any later version.

 This library is distributed in the hope that it will be useful, but 
 WITHOUT ANY WARRANTY; without even the implied warranty of 
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License 
 along with this library; if not, write to the Free Software Foundation, 
 Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
 ===========================================================================*/

package net.sf.okapi.steps.copyormove;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.ListSelectionPart;

@EditorFor(Parameters.class)
public class Parameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String COPYOPTION = "copyOption";
	private static final String MOVE = "move";
	
	public static final String COPYOPTION_OVERWRITE = "overwrite";
	public static final String COPYOPTION_BACKUP = "backup";
	public static final String COPYOPTION_SKIP = "skip";
	
	public Parameters() {
		super();
	}

	@Override
	public void reset() {
		super.reset();
		setCopyOption(COPYOPTION_OVERWRITE);
		setMove(false);
	}

	public boolean isMove() {
		return getBoolean(MOVE);
	}

	public void setMove(boolean move) {
		setBoolean(MOVE, move);
	}

	public String getCopyOption() {
		return getString(COPYOPTION);
	}
	
	public void setCopyOption(String copyOption) {
		setString(COPYOPTION, copyOption);			
	}

	@Override
	public ParametersDescription getParametersDescription() {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(COPYOPTION, "Choose copy protection method:", null);
		desc.add(MOVE, "Move files?", null);
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription(ParametersDescription paramDesc) {
		EditorDescription desc = new EditorDescription("Copy Or Move", true, false);
		String[] values = {COPYOPTION_OVERWRITE, COPYOPTION_BACKUP, COPYOPTION_SKIP};
		String[] labels = {"Overwrite existing files", "Backup existing files", "Skip copy/move"};
		ListSelectionPart lsp = desc.addListSelectionPart(paramDesc.get(COPYOPTION), values);
		lsp.setChoicesLabels(labels);
		lsp.setListType(ListSelectionPart.LISTTYPE_DROPDOWN);
		desc.addCheckboxPart(paramDesc.get(MOVE));
		return desc;
	}
}
