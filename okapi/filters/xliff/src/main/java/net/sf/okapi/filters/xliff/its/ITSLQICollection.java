/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.xliff.its;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.xml.stream.events.Attribute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents the &lt;its:locQualityIssues/> element.
 */
public class ITSLQICollection {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private String resource;
	private String xmlid, version;
	private List<ITSLQI> issues = new LinkedList<ITSLQI>();

	public ITSLQICollection(Iterator<Attribute> attrs, String resource) {
		this.resource = resource;
		while (attrs.hasNext()) {
			Attribute attr = attrs.next();
			String prefix = attr.getName().getPrefix();
			String name = attr.getName().getLocalPart();
			String value = attr.getValue();
			if (!prefix.isEmpty()) {
				name = prefix + ":" + name;
			}

			if ( name.equals("xml:id") ) {
				xmlid = value;
			}
			else if ( name.equals("version") ) {
				version = value;
			}
			else if ( !name.equals("xmlns:its") ) {
				logger.warn("Unrecognized attribute '{}'.", name);
			}
		}
	}

	public ITSLQICollection(List<ITSLQI> issues, 
			String resource, String xmlid, String version) {
		this.issues = issues;
		this.resource = resource;
		this.xmlid = xmlid;
		this.version = version;
	}
	
	public void addLQI (Iterator<Attribute> attrs) {
		issues.add(new ITSLQI(attrs));
	}

	public String getXMLId () {
		return this.xmlid;
	}

	public String getVersion () {
		return version;
	}

	public String getURI () {
		return this.resource+"#"+this.xmlid;
	}

	public Iterator<ITSLQI> iterator () {
		return issues.iterator();
	}
}
