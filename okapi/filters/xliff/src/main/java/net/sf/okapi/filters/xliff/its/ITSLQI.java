/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.xliff.its;

import java.util.Iterator;

import javax.xml.stream.events.Attribute;

import net.sf.okapi.common.Namespaces;
import net.sf.okapi.common.annotation.GenericAnnotation;
import net.sf.okapi.common.annotation.IssueAnnotation;
import net.sf.okapi.common.annotation.IssueType;
import net.sf.okapi.filters.xliff.XLIFFITSFilterExtension;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents the <its:locQualityIssue/> element.
 */
public class ITSLQI {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private IssueAnnotation ann = new IssueAnnotation();

	public ITSLQI (Iterator<Attribute> attrs) {

		String itsType = null;
		while ( attrs.hasNext() ) {
			Attribute attr = attrs.next();
			String prefix = attr.getName().getPrefix();
			String name = attr.getName().getLocalPart();
			String ns = attr.getName().getNamespaceURI();
			String value = attr.getValue();
			String prefixedName = name;
			if ( !prefix.isEmpty() && !prefix.equals("its") ) {
				prefixedName = prefix + ":" + name;
			}

			// Read ITS attributes
			// (We assume empty namespace is ITS since this is inside a standoff item)
			if ( ns.isEmpty() || ns.equals(Namespaces.ITS_NS_URI)  ) {
				if ( name.equals("locQualityIssueType") ) {
					ann.setITSType(value);
					itsType = value; // Remember the original ITS type
				}
				else if ( name.equals("locQualityIssueComment") ) {
					ann.setComment(value);
				}
				else if ( name.equals("locQualityIssueSeverity") ) {
					ann.setSeverity(Double.parseDouble(value));
				}
				else if ( name.equals("locQualityIssueProfileRef") ) {
					ann.setProfileRef(value);
				}
				else if ( name.equals("locQualityIssueEnabled") ) {
					ann.setEnabled(value.equals("yes"));
				}
				else {
					logger.warn("Unknown attribute '{}'.", prefixedName);
				}
			}
			else if ( ns.equals(Namespaces.NS_XLIFFOKAPI) ) {
				// Read the extensions
				if ( name.equals("lqiType") ) {
					ann.setIssueType(IssueType.valueOf(value));
					// There an ITS type was set before, make sure we keep that value
					if ( itsType != null ) {
						ann.setITSType(itsType);
					}
				}
				else if ( name.equals("lqiPos") ) {
					int[] positions = XLIFFITSFilterExtension.parseXLQIPos(value, logger);
					ann.setSourcePosition(positions[0], positions[1]);
					ann.setTargetPosition(positions[2], positions[3]);
				}
				else if ( name.equals("lqiCodes") ) {
					ann.setCodes(value);
				}
				else if ( name.equals("lqiSegId") ) {
					ann.setSegId(value);
				}
				else {
					logger.warn("Unknown attribute '{}'.", prefixedName);
				}
			}
			else {
				logger.warn("Unknown attribute '{}'.", prefixedName);
			}
		}
	}
	
	public ITSLQI (IssueAnnotation ann) {
		this.ann = (IssueAnnotation) ann.clone();
	}

	public String getType () {
		return ann.getITSType();
	}

	public double getSeverity () {
		return ann.getSeverity();
	}

	public String getComment () {
		return ann.getComment();
	}

	public String getProfileRef () {
		return ann.getProfileRef();
	}

	public boolean getEnabled () {
		return ann.getEnabled();
	}
	
	/**
	 * Gets the annotation for this LQI object.
	 * @return the annotation, or null if there is no fields set (annotation is empty).
	 */
	public GenericAnnotation getAnnotation () {
		if ( ann.getFieldCount() == 0 ) return null;
		else return ann;
	}
}