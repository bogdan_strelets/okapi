package net.sf.okapi.filters.openxml;

import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;

import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.*;

public class TestExcelWorksheet extends XMLTestCase {
	private XMLInputFactory inputFactory = XMLInputFactory.newInstance();
	private XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();

	@Test
	public void test() throws Exception {
		SharedStringMap ssm = new SharedStringMap();
		ExcelWorksheet parser = new ExcelWorksheet(XMLEventFactory.newInstance(), ssm, new ExcelStyles(),
								new HashSet<String>(), new HashSet<String>());
		XMLEventReader reader = inputFactory.createXMLEventReader(getClass().getResourceAsStream("/xlsx_parts/sheet1.xml"), "UTF-8");
		StringWriter sw = new StringWriter();
		XMLEventWriter writer = outputFactory.createXMLEventWriter(sw);
		parser.parse(reader, writer);
		reader.close();
		writer.close();
		assertXMLEqual(new InputStreamReader(
						getClass().getResourceAsStream("/xlsx_parts/gold/Rewritten_sheet1.xml"),
						StandardCharsets.UTF_8), new StringReader(sw.toString()));
	}

	@Test
	public void testExcludeColors() throws Exception {
		SharedStringMap ssm = new SharedStringMap();
		ExcelStyles styles = new ExcelStyles();
		styles.parse(inputFactory.createXMLEventReader(
				getClass().getResourceAsStream("/xlsx_parts/rgb_styles.xml"), "UTF-8"));
		Set<String> excludedColors = new HashSet<String>();
		excludedColors.add("FF800000");
		excludedColors.add("FFFF0000");
		ExcelWorksheet parser = new ExcelWorksheet(XMLEventFactory.newInstance(), ssm, styles,
				new HashSet<String>(), excludedColors);
		XMLEventReader reader = inputFactory.createXMLEventReader(
				getClass().getResourceAsStream("/xlsx_parts/rgb_sheet1.xml"), "UTF-8");
		StringWriter sw = new StringWriter();
		XMLEventWriter writer = outputFactory.createXMLEventWriter(sw);
		parser.parse(reader, writer);
		reader.close();
		writer.close();
		List<SharedStringMap.Entry> entries = ssm.getEntries();
		assertTrue(entries.get(0).getExcluded());  // excluded due to FF800000
		assertTrue(entries.get(1).getExcluded());  // excluded due to FFFF0000
		assertFalse(entries.get(2).getExcluded());
		assertFalse(entries.get(3).getExcluded());
		assertFalse(entries.get(4).getExcluded());
		assertFalse(entries.get(5).getExcluded());
		assertFalse(entries.get(6).getExcluded());
		assertFalse(entries.get(7).getExcluded());
		assertFalse(entries.get(8).getExcluded());
		assertFalse(entries.get(9).getExcluded());
	}
}
