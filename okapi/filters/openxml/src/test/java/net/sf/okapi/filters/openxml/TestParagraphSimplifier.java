package net.sf.okapi.filters.openxml;

import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;

import org.custommonkey.xmlunit.Diff;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestParagraphSimplifier {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private XMLInputFactory inputFactory = XMLInputFactory.newInstance();
	private XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
	private XMLEventFactory eventFactory = XMLEventFactory.newInstance();

	@Test
	public void testSimplifier() throws Exception {
		simplifyAndCheckFile("document-simple.xml");
		simplifyAndCheckFileAggressive("document-simple.xml");
	}

	@Test
	public void testDontMergeWhenPropertiesDontMatch() throws Exception {
		simplifyAndCheckFile("document-prop_mismatch.xml");
	}

	@Test
	public void testWithTabs() throws Exception {
		simplifyAndCheckFile("document-multiple_tabs.xml");
	}

	@Test
	public void testHeaderWithConsecutiveTabs() throws Exception {
		simplifyAndCheckFile("header-tabs.xml");
	}

	@Test
	public void testTextBoxes() throws Exception {
		simplifyAndCheckFile("document-textboxes.xml");
	}

	@Test
	public void testRuby() throws Exception {
		simplifyAndCheckFile("document-ruby.xml");
	}

	@Test
	public void testSlide() throws Exception {
		simplifyAndCheckFile("slide-sample.xml");
	}

	@Test
	public void testInstrText() throws Exception {
		simplifyAndCheckFile("document-instrText.xml");
	}

	@Test
	public void testAltContent() throws Exception {
		simplifyAndCheckFile("document-altcontent.xml");
	}

	@Test
	public void testPreserveSpaceReset() throws Exception {
		simplifyAndCheckFile("document-preserve.xml");
	}

	@Test
	public void testStripLastRenderedPagebreak() throws Exception {
		simplifyAndCheckFile("document-pagebreak.xml");
	}

	@Test
	public void testStripSpellingGrammarError() throws Exception {
		simplifyAndCheckFile("document-spelling.xml");
	}

	@Test
	public void testLangAttributeAndEmptyRunPropertyMerging() throws Exception {
		simplifyAndCheckFile("document-lang.xml");
	}

	@Test
	public void testDontConsolidateMathRuns() throws Exception {
		simplifyAndCheckFile("slide-formulas.xml");
	}

	@Test
	public void testAggressiveSpacingTrimming() throws Exception {
		simplifyAndCheckFile("document-spacing.xml");
		simplifyAndCheckFileAggressive("document-spacing.xml");
	}

	@Test
	public void testAggressiveVertAlignTrimming() throws Exception {
		simplifyAndCheckFileAggressive("document-vertAlign.xml");
	}

	@Test
	public void testGoBackBookmark() throws Exception {
		simplifyAndCheckFile("document-goback.xml");
	}

	@Test
	public void testTab() throws Exception {
		simplifyAndCheckFileTabAsChar("document-tab.xml");
	}

	public Path simplifyFile(String name) throws Exception {
		return simplifyFile(name, false, false);
	}

	public Path simplifyFileAggressively(String name) throws Exception {
		return simplifyFile(name, true, false);
	}

	public Path simplifyFile(String name, boolean aggressiveTrimming, boolean tabAsChar) throws Exception {
		XMLEventReader xmlReader = inputFactory.createXMLEventReader(
				getClass().getResourceAsStream("/parts/simplifier/" + name), "UTF-8");
		Path temp = Files.createTempFile("simplify", ".xml");
		//System.out.println("Writing simplifed " + name + " (aggressive=" + aggressiveTrimming + ") to " + temp);
		XMLEventWriter xmlWriter = outputFactory.createXMLEventWriter(
				Files.newBufferedWriter(temp, StandardCharsets.UTF_8));
		ConditionalParameters params = new ConditionalParameters();
		params.setCleanupAggressively(aggressiveTrimming);
		params.setAddTabAsCharacter(tabAsChar);
		ParagraphSimplifier simplifier = new ParagraphSimplifier(xmlReader, xmlWriter, eventFactory, params);
		simplifier.process();
		xmlReader.close();
		xmlWriter.close();
		return temp;
	}

	// Simplify 
	//   src/test/resources/parts/simplifier/[name]
	// And compare to
	//   src/test/resources/gold/parts/simplifier/[name]
	public void simplifyAndCheckFile(String name) throws Exception {
		simplifyAndCheckFile(name, "/gold/parts/simplifier/", false, false);
	}
	public void simplifyAndCheckFileAggressive(String name) throws Exception {
		simplifyAndCheckFile(name, "/gold/parts/simplifier/aggressive/", true, false);
	}
	public void simplifyAndCheckFileTabAsChar(String name) throws Exception {
		simplifyAndCheckFile(name, "/gold/parts/simplifier/tabAsChar/", false, true);
	}
	public void simplifyAndCheckFile(String name, String goldDir, boolean aggressive, boolean tabAsChar) throws Exception {
		Path temp = simplifyFile(name, aggressive, tabAsChar);
		try (Reader gold = getGoldReader(goldDir, name);
			 Reader out = Files.newBufferedReader(temp, StandardCharsets.UTF_8)) {
			Diff diff = new Diff(gold, out);
			if (!diff.similar()) {
				StringBuffer sb = new StringBuffer("'" + name + "' gold file does not match " + temp + ":");
				diff.appendMessage(sb);
				LOGGER.warn(sb.toString());
				Assert.fail();
			}
		}
		Files.delete(temp);
	}

	private Reader getGoldReader(String goldDir, String name) {
		return new InputStreamReader(getClass().getResourceAsStream(goldDir + name),
									 StandardCharsets.UTF_8);
	}
}