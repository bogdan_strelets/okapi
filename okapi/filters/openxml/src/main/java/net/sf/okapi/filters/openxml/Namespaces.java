package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;

public enum Namespaces {
	XML("http://www.w3.org/XML/1998/namespace"),
	Rels("http://schemas.openxmlformats.org/package/2006/relationships"),
	ContentTypes("http://schemas.openxmlformats.org/package/2006/content-types"),
	WordProcessingML("http://schemas.openxmlformats.org/wordprocessingml/2006/main"),
	SpreadsheetML("http://schemas.openxmlformats.org/spreadsheetml/2006/main"),
	PresentationML("http://schemas.openxmlformats.org/presentationml/2006/main"),
	DrawingML("http://schemas.openxmlformats.org/drawingml/2006/main"),
	Math("http://schemas.openxmlformats.org/officeDocument/2006/math"),
	// Seperate namespace that is used, somewhat strangely, to refer to rel data
	// (e.g., rel IDs) from non-rel sources.
	DocumentRels("http://schemas.openxmlformats.org/officeDocument/2006/relationships");

	private String nsURI;
	Namespaces(String nsURI) {
		this.nsURI = nsURI;
	}

	public String getURI() {
		return nsURI;
	}

	public QName getQName(String localPart) {
		return new QName(nsURI, localPart);
	}

	public String getDerivedURI(String path) {
		StringBuilder sb = new StringBuilder(nsURI);
		if (!path.startsWith("/")) {
			sb.append("/");
		}
		sb.append(path);
		return sb.toString();
	}

	public boolean containsName(QName name) {
		return nsURI.equals(name.getNamespaceURI());
	}
}
