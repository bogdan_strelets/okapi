package net.sf.okapi.filters.openxml;

import java.io.IOException;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * Class to parse an individual worksheet and update the shared string
 * data based on worksheet cells and exclusion information.
 */
public class ExcelWorksheet {
	private XMLEventFactory eventFactory;
	private SharedStringMap stringTable;
	private Set<String> excludedColumns;
	private Set<String> excludedColors;
	private ExcelStyles styles;

	public ExcelWorksheet(XMLEventFactory eventFactory, SharedStringMap stringTable,
						  ExcelStyles styles, Set<String> excludedColumns,
						  Set<String> excludedColors) {
		this.eventFactory = eventFactory;
		this.stringTable = stringTable;
		this.styles = styles;
		this.excludedColumns = excludedColumns;
		this.excludedColors = excludedColors;
	}

	static final QName ROW = Namespaces.SpreadsheetML.getQName("row");
	static final QName CELL = Namespaces.SpreadsheetML.getQName("c");
	static final QName VALUE = Namespaces.SpreadsheetML.getQName("v");
	static final QName CELL_LOCATION = new QName("r");
	static final QName CELL_TYPE = new QName("t");
	static final QName CELL_STYLE = new QName("s");
	void parse(XMLEventReader reader, XMLEventWriter writer) throws IOException, XMLStreamException {
		boolean excluded = false;
		boolean inValue = false;
		boolean isSharedString = false;
		while (reader.hasNext()) {
			XMLEvent e = reader.nextEvent();
			if (e.isStartElement()) {
				StartElement el = e.asStartElement();
				if (el.getName().equals(CELL)) {
					// We only care about cells with @t="s", indicating a shared string
					Attribute typeAttr = el.getAttributeByName(CELL_TYPE);
					if (typeAttr != null && typeAttr.getValue().equals("s")) {
						String currentColumn = getLocationColumn(el.getAttributeByName(CELL_LOCATION).getValue());
						excluded = excludedColumns.contains(currentColumn);
						isSharedString = true;
					}
					Attribute styleAttr = el.getAttributeByName(CELL_STYLE);
					if (styleAttr != null) {
						int styleIndex = Integer.valueOf(styleAttr.getValue());
						ExcelStyles.CellStyle style = styles.getCellStyle(styleIndex);
						// I'm going to start with a naive implementation that should
						// basically be fine, but not ideal if we're excluding large numbers
						// of colors.
						for (String excludedColor : excludedColors) {
							if (style.fill.matchesColor(excludedColor)) {
								excluded = true;
								break;
							}
						}
					}
				}
				else if (el.getName().equals(VALUE)) {
					inValue = true;
				}
			}
			else if (e.isEndElement()) {
				EndElement el = e.asEndElement();
				if (el.getName().equals(CELL)) {
					excluded = false;
					isSharedString = false;
				}
				else if (el.getName().equals(VALUE)) {
					inValue = false;
				}
			}
			else if (e.isCharacters() && inValue && isSharedString) {
				int origIndex = getSharedStringIndex(e.asCharacters().getData());
				int newIndex = stringTable.createEntryForString(origIndex, excluded).getNewIndex();
				// Replace the event with one that contains the new index
				e = eventFactory.createCharacters(String.valueOf(newIndex));
			}
			writer.add(e);
		}
	}

	private static int getSharedStringIndex(String value) {
		try {
			return Integer.valueOf(value);
		}
		catch (NumberFormatException e) {
			throw new IllegalStateException("Unexpected shared string index '" + value + "'");
		}
	}
	private static String getLocationColumn(String location) {
		char buf[] = location.toCharArray();
		for (int i = 0; i < buf.length; i++) {
			if (Character.isDigit(buf[i])) {
				return location.substring(0, i);
			}
		}
		// I don't think this should never happen, so fail fast
		throw new IllegalStateException("Unexpected worksheet cell location '" + location + "'");
	}
}
