package net.sf.okapi.filters.openxml;

import static net.sf.okapi.filters.openxml.ParseType.MSWORD;
import static net.sf.okapi.filters.openxml.ParseType.MSWORDCHART;
import static net.sf.okapi.filters.openxml.ParseType.MSWORDDOCPROPERTIES;
import static net.sf.okapi.filters.openxml.ContentTypes.Types.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;

import javax.xml.stream.XMLStreamException;

public class WordDocument extends DocumentType {

	public WordDocument(OpenXMLZipFile zipFile, ConditionalParameters params) {
		super(zipFile, params);
	}

	@Override
	public ParseType getDefaultParseType() {
		return ParseType.MSWORD;
	}

	@Override
	public void initialize() throws IOException, XMLStreamException {
	}

	@Override
	public OpenXMLPartHandler getHandlerForFile(ZipEntry entry,
			String contentType, boolean squishable) {

		// Check to see if this is non-translatable
		if (!isTranslatableType(entry.getName(), contentType)) {
			return new NonTranslatablePartHandler(getZipFile(), entry);
		}
                
		OpenXMLContentFilter openXMLContentFilter =
				new OpenXMLContentFilter(getZipFile().getFactories(), getParams());
		openXMLContentFilter.setPartName(entry.getName());
		ParseType parseType = ParseType.MSWORD;
		boolean shouldSquishPart = false;
		// Handle a few specialized part types
		if (Word.MAIN_DOCUMENT_TYPE.equals(contentType)) {
			openXMLContentFilter.setBInMainFile(true);
			// XXX I don't understand this line.
			parseType = squishable ? MSWORD : MSWORDDOCPROPERTIES;
			shouldSquishPart = squishable;
		}
		else if (Word.SETTINGS_TYPE.equals(contentType)) {
			openXMLContentFilter.setBInSettingsFile(true);
		}
		else if (Drawing.CHART_TYPE.equals(contentType)) {
			parseType = MSWORDCHART;
		}
		else if (Common.CORE_PROPERTIES_TYPE.equals(contentType)) {
			parseType = MSWORDDOCPROPERTIES;
		}
		openXMLContentFilter.setUpConfig(parseType);

		// From openSubDocument
		if (!getParams().getTranslateWordHidden() || !getParams().getTranslateWordAllStyles()) {
			openXMLContentFilter.setTsExcludeWordStyles(getParams().tsExcludeWordStyles);
		}
		return new StandardPartHandler(openXMLContentFilter, getParams(), getZipFile(), entry, shouldSquishPart);
	}

	private boolean isTranslatableType(String entryName, String type) {
		if (!entryName.endsWith(".xml")) return false;
		if (type.equals(Word.MAIN_DOCUMENT_TYPE)) return true;
		//if (getParams().getTranslateWordHidden() && type.equals(Word.STYLES_TYPE)) return true;
		if (type.equals(Word.STYLES_TYPE)) return true;
		if (getParams().getTranslateDocProperties() && type.equals(Common.CORE_PROPERTIES_TYPE)) return true;
		if (getParams().getTranslateWordHeadersFooters()) {
			if (type.equals(Word.HEADER_TYPE) || type.equals(Word.FOOTER_TYPE)) return true;
		}
		if (getParams().getTranslateComments() && type.equals(Word.COMMENTS_TYPE)) return true;
		if (type.equals(Word.SETTINGS_TYPE)) return true;
		if (type.equals(Word.ENDNOTES_TYPE)) return true;
		if (type.equals(Word.FOOTNOTES_TYPE)) return true;
		if (type.equals(Word.GLOSSARY_TYPE)) return true;
		if (type.equals(Drawing.CHART_TYPE)) return true;
		if (type.equals(Drawing.DIAGRAM_TYPE)) return true;
		return false;
	}

	@Override
	public Enumeration<? extends ZipEntry> getZipFileEntries() {
		List<? extends ZipEntry> list = Collections.list(getZipFile().entries());
		List<String> additionalParts = new ArrayList<String>();
		additionalParts.add("word/styles.xml");
		additionalParts.add("word/_rels/document.xml.rels");
		additionalParts.add("word/document.xml");
		Collections.sort(list, new ZipEntryComparator(additionalParts));
		return Collections.enumeration(list);
	}
}
