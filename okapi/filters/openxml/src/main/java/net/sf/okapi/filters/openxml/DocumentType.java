package net.sf.okapi.filters.openxml;

import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;

import javax.xml.stream.XMLStreamException;

public abstract class DocumentType {
	private OpenXMLZipFile zipFile;
	private ConditionalParameters params;

	public DocumentType(OpenXMLZipFile zipFile, ConditionalParameters params) {
		this.zipFile = zipFile;
		this.params = params;
	}

	protected ConditionalParameters getParams() {
		return params;
	}

	// XXX Still needed?
	public abstract ParseType getDefaultParseType();

	public abstract OpenXMLPartHandler getHandlerForFile(ZipEntry entry, String mediaType,
														 boolean squishable);

	public abstract void initialize() throws IOException, XMLStreamException;

	/**
	 * Return the zip file entries for this document in the order they should be processed.
	 */
	public abstract Enumeration<? extends ZipEntry> getZipFileEntries()
		 throws IOException, XMLStreamException;

	protected OpenXMLZipFile getZipFile() {
		return zipFile;
	}
}
