package net.sf.okapi.filters.openxml;

import java.io.IOException;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;

public interface OpenXMLPartHandler {
	/**
	 * Open this part and perform any initial processing.  Return the
	 * first event for this part.
	 * @param srcLang
	 * @return
	 * @throws IOException
	 */
	Event open(String docId, String subDocId, LocaleId srcLang) throws IOException;

	boolean hasNext();

	Event next();

	void close();

	void logEvent(Event e);
}
