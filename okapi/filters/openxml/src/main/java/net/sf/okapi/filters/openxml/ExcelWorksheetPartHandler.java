package net.sf.okapi.filters.openxml;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Set;
import java.util.zip.ZipEntry;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;

public class ExcelWorksheetPartHandler extends NonTranslatablePartHandler {
	private ConditionalParameters cparams;
	private int sheetNumber;
	private SharedStringMap ssm;
	private ExcelStyles styles;

	ExcelWorksheetPartHandler(OpenXMLZipFile zipFile, ZipEntry entry, SharedStringMap ssm, ExcelStyles styles,
						 	  int sheetNumber, ConditionalParameters cparams) {
		super(zipFile, entry);
		this.sheetNumber = sheetNumber;
		this.ssm = ssm;
		this.cparams = cparams;
		this.styles = styles;
	}

	@Override
	protected String getModifiedContent() {
		try {
			StringWriter sw = new StringWriter();
			Set<String> excludedColumns = cparams.findExcludedColumnsForSheetNumber(sheetNumber);
			XMLEventReader r = getZipFile().getInputFactory().createXMLEventReader(
					getZipFile().getPartReader(getEntry().getName()));
			XMLEventWriter w = getZipFile().getOutputFactory().createXMLEventWriter(sw);
			new ExcelWorksheet(getZipFile().getEventFactory(), ssm, styles, excludedColumns,
							   cparams.tsExcelExcludedColors).parse(r, w);
			return sw.toString();
		}
		catch (IOException e) {
			throw new OkapiBadFilterInputException(e);
		} catch (XMLStreamException e) {
			throw new OkapiBadFilterInputException(e);
		}
	}
}
