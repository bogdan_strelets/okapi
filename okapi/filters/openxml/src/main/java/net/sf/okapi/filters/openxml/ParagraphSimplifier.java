package net.sf.okapi.filters.openxml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simplifies markup within the paragraphs or other rich
 * content structure or a document part, by
 * - Stripping revision ID (rsid*) and powerpoint spelling error information
 * - Merging consecutive runs with equivalent run properties
 * - Merge consecutive text elements
 */
public class ParagraphSimplifier {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private XMLEventReader xmlReader;
	private XMLEventWriter xmlWriter;
	private XMLEventFactory eventFactory;
	private ConditionalParameters params;

	public ParagraphSimplifier(XMLEventReader xmlReader, XMLEventWriter xmlWriter, XMLEventFactory eventFactory,
							   ConditionalParameters params) {
		this.xmlReader = xmlReader;
		this.xmlWriter = xmlWriter;
		this.eventFactory = eventFactory;
		this.params = params;
	}

	public void process() throws XMLStreamException {
		while (xmlReader.hasNext()) {
			XMLEvent e = xmlReader.nextEvent();
			if (isBlockStartEvent(e)) {
				List<XMLEvent> blockEvents = processBlock(e.asStartElement(), xmlReader);
				flushEvents(blockEvents);
			}
			else {
				if (isStartElement(e, LOCAL_SECTIONPROPS)) {
					e = stripRevisionAttrs(e.asStartElement());
				}
				xmlWriter.add(e);
			}
		}
	}

	// Someday we may want this to return a block structure
	private List<XMLEvent> processBlock(StartElement startEvent, XMLEventReader events) throws XMLStreamException {
		log("startBlock: " + startEvent);
		List<XMLEvent> blockEvents = new ArrayList<>();
		Run previousRun = null;
		blockEvents.add(stripRevisionAttrs(startEvent));
		while (events.hasNext()) {
			XMLEvent e = events.nextEvent();
			if (isRunStartEvent(e)) {
				Run r = processRun(e.asStartElement(), events);
				if (previousRun != null) {
					if (previousRun.canMerge(r)) {
						previousRun.merge(r);
					}
					else {
						blockEvents.addAll(previousRun.getEvents());
						previousRun = r;
					}
				}
				else {
					previousRun = r;
				}
			}
			else {
				// Strip elements we don't care about
				if (e.isStartElement()) {
					if (canStripParagraphElement(e.asStartElement(), events)) {
						continue;
					}
					if (canStripBookmark(e.asStartElement(), events)) {
						continue;
					}
				}
				// Flush any outstanding run if there's any markup
				if (previousRun != null && (e.isStartElement() || e.isEndElement())) {
					blockEvents.addAll(previousRun.getEvents());
					previousRun = null;
				}
				// Trim non-essential whitespace
				if (!isWhitespace(e)) {
					blockEvents.add(e);
				}
				// Check for end of block
				if (e.isEndElement() && startEvent.getName().equals(e.asEndElement().getName())) {
					log("End block: " + e);
					return blockEvents;
				}
			}
		}
		throw new IllegalStateException("Invalid content? Unterminated paragraph");
	}

	private Run processRun(StartElement startEvent, XMLEventReader events) throws XMLStreamException {
		Run r = new Run(startEvent);
		log("startRun: " + startEvent);
		// rPr is either the first child, or not present (section 17.3.2)
		XMLEvent firstChild = events.nextTag();
		if (isRunPropsStartEvent(firstChild)) {
			processRunProps(r, firstChild.asStartElement(), events, false);
		}
		else if (isEndElement(firstChild, startEvent)) {
			// Empty run!
			r.endRun(firstChild.asEndElement());
			log("endRun");
			return finalizeRun(r);
		}
		else {
			// No properties section
			r.processRunBody(firstChild, events);
		}
		while (events.hasNext()) {
			XMLEvent e = events.nextEvent();
			log("processRun: " + e);
			if (isEndElement(e, startEvent)) {
				r.endRun(e.asEndElement());
				log("endRun");
				return finalizeRun(r);
			}
			else {
				// Handle non-properties (run body) content
				r.processRunBody(e, events);
			}
		}
		throw new IllegalStateException("Invalid content? Unterminated run");
	}

	private Run finalizeRun(Run r) throws XMLStreamException {
		XMLEventReader oldRunPropEvents = new XMLListEventReader(r.runPropsEvents);
		r.runPropsEvents = new ArrayList<>();
		r.runPropCount = 0;
		if (oldRunPropEvents.hasNext()) {
			XMLEvent first = oldRunPropEvents.nextEvent();
			processRunProps(r, first.asStartElement(), oldRunPropEvents, true);
		}
		return r;
	}

	private void processRunProps(Run r, StartElement startEvent, XMLEventReader events,
								 boolean stripUselessProperties) throws XMLStreamException {
		r.startRunProps(stripRevisionAttrs(startEvent));
		// If this is a DrawingML run, count the properties that are present as
		// attributes in the <a:rPr> element itself.
		if (Namespaces.DrawingML.containsName(startEvent.getName())) {
			r.runPropCount += countAttributes(startEvent.getAttributes());
		}
		while (events.hasNext()) {
			XMLEvent e = events.nextEvent();
			if (isEndElement(e, startEvent)) {
				r.endRunProps(e.asEndElement());
				return;
			}
			if (e.isStartElement()) {
				if (stripUselessProperties && r.isStrippableRunPropertyElement(e.asStartElement())) {
					skipElement(e.asStartElement(), events);
				}
				else {
					// This gathers the whole event.
					r.addRunProp(e.asStartElement(), events);
				}
			}
			// Discard -- make sure we're not discarding meaningful data
			else if (e.isCharacters() && !isWhitespace(e)) {
				throw new IllegalStateException("Discarding non-whitespace rPr characters " +
							e.asCharacters().getData());
			}
		}
		throw new IllegalStateException("Invalid content? Unterminated run properties");
	}

	class Run {
		private StartElement startEvent;
		private EndElement endEvent;
		private List<XMLEvent> runPropsEvents = new ArrayList<>();
		private List<XMLEvent> runBodyEvents = new ArrayList<>();
		private QName textName = null;
		private boolean isTextPreservingWhitespace = false;
		private StringBuilder textContent = new StringBuilder();
		private boolean hadAnyText = false, hadNonWhitespaceText = false;
		private int runPropCount = 0;

		public Run(StartElement startEvent) {
			this.startEvent = stripRevisionAttrs(startEvent);
		}

		public boolean canMerge(Run otherRun) {
			// Merging runs in the math namespace can sometimes corrupt formulas,
			// so don't do it.
			if (Namespaces.Math.containsName(startEvent.getName())) {
				return false;
			}
			// Runs that have no properties specified can always be merged
			if (runPropCount == 0 && otherRun.runPropCount == 0) {
				return true;
			}
			// XMLEvent doesn't implement equals() in the default implementation,
			// so we compare by hand.
			if (runPropsEvents.size() != otherRun.runPropsEvents.size()) {
				return false;
			}
			for (int i = 0; i < runPropsEvents.size(); i++) {
				if (!eventEquals(runPropsEvents.get(i), otherRun.runPropsEvents.get(i))) {
					return false;
				}
			}
			return true;
		}

		public boolean isStrippableRunPropertyElement(StartElement el) {
			if (el.getName().equals(RUN_CONTENT_LANGUAGE)) {
				return true;
			}
			if (params.getCleanupAggressively()) {
				if (el.getName().equals(RUN_CONTENT_SPACING)) {
					return true;
				}
				if (el.getName().equals(RUN_CONTENT_VERTALIGN) && !hadNonWhitespaceText) {
					return true;
				}
			}
			return false;
		}

		/**
		 * Merge the property body.  This is something of a mess, since
		 * this may cause tetris-style collapsing of consecutive text across
		 * the two runs. To handle this correctly, we concatenate the two run
		 * bodies, then re-process them.
		 */
		public void merge(Run other) throws XMLStreamException {
			List<XMLEvent> newBodyEvents = new ArrayList<>(runBodyEvents);
			isTextPreservingWhitespace = isTextPreservingWhitespace ? true : other.isTextPreservingWhitespace;
			newBodyEvents.addAll(other.runBodyEvents);
			runBodyEvents.clear();
			XMLEventReader events = new XMLListEventReader(newBodyEvents);
			while (events.hasNext()) {
				addRunBody(events.nextEvent(), events);
			}
			// Flush any outstanding text
			flushText();
		}

		public void startRunProps(StartElement e) {
			runPropsEvents.add(e);
		}

		public void addRunProp(StartElement startEl, XMLEventReader events) throws XMLStreamException {
			// Gather elements up to the end
			runPropsEvents.add(startEl);
			runPropCount++;
			while (events.hasNext()) {
				XMLEvent e = events.nextEvent();
				runPropsEvents.add(e);
				if (isEndElement(e, startEl)) {
					return;
				}
			}
		}

		public void endRunProps(EndElement e) {
			runPropsEvents.add(e);
		}

		public void processRunBody(XMLEvent e, XMLEventReader events) throws XMLStreamException {
			if (isBlockStartEvent(e)) {
				log("Nested block start event: " + e);
				flushText();
				// XXX Don't require a child instance?  Will require some refactoring
				ParagraphSimplifier nested = new ParagraphSimplifier(xmlReader, xmlWriter,
												eventFactory, params);
				List<XMLEvent> nestedEvents = nested.processBlock(e.asStartElement(), events);
				runBodyEvents.addAll(nestedEvents);
			}
			else {
				addRunBody(e, xmlReader);
			}
		}

		public void addRunBody(XMLEvent e, XMLEventReader events) throws XMLStreamException {
			if (isTextStartEvent(e)) {
				processText(e.asStartElement(), events);
			}
			else if (isTabStartEvent(e) && params.getAddTabAsCharacter()) {
				addRawText("\t", e.asStartElement(), events);
			}
			else if (isLineBreakStartEvent(e) && params.getAddLineSeparatorCharacter()) {
				addRawText("\n", e.asStartElement(), events);
			}
			else if (e.isStartElement() && isStrippableRunBodyElement(e.asStartElement())) {
				// Consume to the end of the element
				skipElement(e.asStartElement(), events);
			}
			else if (!isWhitespace(e) || inPreservingWhitespaceElement()) {
				flushText();
				isTextPreservingWhitespace = false;
				runBodyEvents.add(e);
			}
		}

		/**
		 * Handle cases like <w:instrText> -- non-text elements that we may need
		 * to obey xml:space="preserve" on.
		 */
		private boolean inPreservingWhitespaceElement() {
			for (int i = runBodyEvents.size() - 1; i >= 0; i--) {
				if (runBodyEvents.get(i).isStartElement()) {
					return hasPreserveWhitespace((StartElement)runBodyEvents.get(i));
				}
			}
			return false;
		}

		private void processText(StartElement startEvent, XMLEventReader events) throws XMLStreamException {
			hadAnyText = true;
			// Merge the preserve whitespace flag
			isTextPreservingWhitespace = isTextPreservingWhitespace ? true :
					hasPreserveWhitespace(startEvent);
			if (textName == null) {
				textName = startEvent.getName();
			}
			while (events.hasNext()) {
				XMLEvent e = events.nextEvent();
				if (isEndElement(e, startEvent)) {
					return;
				}
				else if (e.isCharacters()) {
					String text = e.asCharacters().getData();
					if (text.trim().length() > 0) {
						hadNonWhitespaceText = true;
					}
					textContent.append(text);
				}
			}
		}

		private void flushText() {
			// It seems like there may be a bug where presml runs need to have
			// an empty <a:t/> at a minimum.
			if (hadAnyText) {
				XMLEvent start = createTextStart();
				runBodyEvents.add(start);
				runBodyEvents.add(eventFactory.createCharacters(textContent.toString()));
				runBodyEvents.add(eventFactory.createEndElement(textName, null));
				textContent.setLength(0);
				hadAnyText = false;
			}
		}

		private XMLEvent createTextStart() {
			return eventFactory.createStartElement(textName,
					isTextPreservingWhitespace ?
							Collections.singleton(
									eventFactory.createAttribute("xml", Namespaces.XML.getURI(), "space", "preserve"))
									.iterator() : null,
							null);
		}

		public void endRun(EndElement e) {
			flushText();
			this.endEvent = e;
		}

		public List<XMLEvent> getEvents() {
			List<XMLEvent> events = new ArrayList<>();
			events.add(startEvent);
			events.addAll(runPropsEvents);
			events.addAll(runBodyEvents);
			events.add(endEvent);
			return events;
		}

		private void addRawText(String text, StartElement startEl, XMLEventReader events) throws XMLStreamException {
			hadAnyText = true;
			textContent.append(text);
			skipElement(startEl, events);
			if (textName == null) {
				textName = new QName(startEl.getName().getNamespaceURI(), LOCAL_TEXT);
			}
		}
	}

	private StartElement stripRevisionAttrs(StartElement el) {
		List<Attribute> newAttrs = new ArrayList<>();
		for (Iterator<?> it = el.getAttributes(); it.hasNext(); ) {
			Attribute a = (Attribute)it.next();
			String localPart = a.getName().getLocalPart();
			// Some of these only appear on particular elements (ie sectPr -> rsidSect). It
			// might be enough to just strip rsid*.  'err' is a PPTX-only flag.
			if (!"rsidR".equals(localPart) && !"rsidRPr".equals(localPart) &&
				!"rsidP".equals(localPart) && !"rsidRDefault".equals(localPart) &&
				!"rsidDel".equals(localPart) && !"rsidSect".equals(localPart) &&
				!"err".equals(localPart)) {
				newAttrs.add(a);
			}
		}
		return eventFactory.createStartElement(el.getName(), newAttrs.iterator(), el.getNamespaces());
	}

	private boolean isStrippableRunBodyElement(StartElement el) {
		return el.getName().equals(CACHED_PAGE_BREAK);
	}

	private boolean canStripParagraphElement(StartElement el, XMLEventReader events) throws XMLStreamException {
		if (el.getName().equals(PROOFING_ERROR)) {
			skipElement(el, events);
			return true;
		}
		return false;
	}

	private String goBackBookmarkId = null;
	private boolean canStripBookmark(StartElement el, XMLEventReader events) throws XMLStreamException {
		if (el.getName().equals(BOOKMARK_START) && "_GoBack".equals(getAttr(el, WPML_NAME))) {
			goBackBookmarkId = getAttr(el, WPML_ID);
			skipElement(el, events);
			return true;
		}
		else if (el.getName().equals(BOOKMARK_END) && Objects.equals(goBackBookmarkId, getAttr(el, WPML_ID))) {
			skipElement(el, events);
			return true;
		}
		return false;
	}

	private void skipElement(StartElement el, XMLEventReader events) throws XMLStreamException {
		QName name = el.getName();
		while (events.hasNext()) {
			XMLEvent e = events.nextTag();
			if (e.isEndElement() && e.asEndElement().getName().equals(name)) {
				return;
			}
		}
		throw new IllegalStateException("Unterminated element " + el + " at " + el.getLocation());
	}

	private void flushEvents(List<XMLEvent> events) throws XMLStreamException {
		for (XMLEvent e : events) {
			try {
				xmlWriter.add(e);
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	static final QName ATTR_XML_SPACE = Namespaces.XML.getQName("space");
	static final QName CACHED_PAGE_BREAK = Namespaces.WordProcessingML.getQName("lastRenderedPageBreak");
	static final QName PROOFING_ERROR = Namespaces.WordProcessingML.getQName("proofErr");
	static final QName BOOKMARK_START = Namespaces.WordProcessingML.getQName("bookmarkStart");
	static final QName BOOKMARK_END = Namespaces.WordProcessingML.getQName("bookmarkEnd");
	static final QName WPML_ID = Namespaces.WordProcessingML.getQName("id");
	static final QName WPML_NAME = Namespaces.WordProcessingML.getQName("name");
	static final QName RUN_CONTENT_LANGUAGE = Namespaces.WordProcessingML.getQName("lang");
	static final QName RUN_CONTENT_SPACING = Namespaces.WordProcessingML.getQName("spacing");
	static final QName RUN_CONTENT_VERTALIGN = Namespaces.WordProcessingML.getQName("vertAlign");
	static final String LOCAL_PARA = "p";
	static final String LOCAL_RUN = "r";
	static final String LOCAL_RUNPROPS = "rPr";
	static final String LOCAL_TEXT = "t";
	static final String LOCAL_TAB = "tab";
	static final String LOCAL_LINEBREAK = "br";
	static final String LOCAL_SECTIONPROPS = "sectPr"; 

	private boolean isBlockStartEvent(XMLEvent e) {
		return isStartElement(e, LOCAL_PARA);
	}

	private boolean isRunStartEvent(XMLEvent e) {
		return isStartElement(e, LOCAL_RUN);
	}

	private boolean isRunPropsStartEvent(XMLEvent e) {
		return isStartElement(e, LOCAL_RUNPROPS);
	}

	private boolean isTextStartEvent(XMLEvent e) {
		return isStartElement(e, LOCAL_TEXT);
	}

	private boolean isTabStartEvent(XMLEvent e) {
		return isStartElement(e, LOCAL_TAB);
	}

	private boolean isLineBreakStartEvent(XMLEvent e) {
		return isStartElement(e, LOCAL_LINEBREAK);
	}

	private boolean isStartElement(XMLEvent e, String expectedLocalPart) {
		return e.isStartElement() ?
				expectedLocalPart.equals(e.asStartElement().getName().getLocalPart()) : false;
	}

	private boolean isEndElement(XMLEvent e, StartElement correspondingStartElement) {
		return e.isEndElement() ?
			e.asEndElement().getName().equals(correspondingStartElement.getName()) : false;
	}

	private boolean hasPreserveWhitespace(StartElement e) {
		return "preserve".equals(getAttr(e, ATTR_XML_SPACE));
	}

	private boolean isWhitespace(XMLEvent e) {
		return (e.isCharacters() && e.asCharacters().getData().trim().isEmpty());
	}

	private int countAttributes(Iterator<?> attrs) {
		int i = 0;
		for ( ; attrs.hasNext(); i++) {
			attrs.next();
		}
		return i;
	}

	private String getAttr(StartElement el, QName name) {
		Attribute attr = el.getAttributeByName(name);
		return (attr != null) ? attr.getValue() : null;
	}

	private boolean eventEquals(XMLEvent e1, XMLEvent e2) {
		if (e1.getEventType() != e2.getEventType()) {
			return false;
		}
		switch (e1.getEventType()) {
		case XMLEvent.START_ELEMENT:
			return startElementEquals(e1.asStartElement(), e2.asStartElement());
		case XMLEvent.END_ELEMENT:
			return e1.asEndElement().getName().equals(e2.asEndElement().getName());
		case XMLEvent.CHARACTERS:
			return e1.asCharacters().getData().equals(e2.asCharacters().getData());
		default:
			return true;
		}
	}

	private boolean startElementEquals(StartElement e1, StartElement e2) {
		return e1.getName().equals(e2.getName()) && attrEquals(e1, e2);
	}

	private boolean attrEquals(StartElement e1, StartElement e2) {
		List<Attribute> a1 = attributesToList(e1);
		List<Attribute> a2 = attributesToList(e2);
		if (a1.size() != a2.size()) return false;
		// This is complicated by Attribute not supporting equals().
		for (Attribute a : a1) {
			boolean foundMatch = false;
			for (Attribute test : a2) {
				if (attrEquals(a, test)) {
					foundMatch = true;
					break;
				}
			}
			if (!foundMatch) return false;
		}
		return true;
	}

	private List<Attribute> attributesToList(StartElement el) {
		List<Attribute> attrs = new ArrayList<>();
		for (Iterator<?> it = el.getAttributes(); it.hasNext(); ) {
			attrs.add((Attribute)it.next());
		}
		return attrs;
	}

	private boolean attrEquals(Attribute a1, Attribute a2) {
		return Objects.equals(a1.getName(), a2.getName()) &&
			   Objects.equals(a1.getValue(), a2.getValue());
	}

	/**
	 * Wrap an event iterator as a true reader; this lets us replay strings
	 * of events during merge.
	 */
	class XMLListEventReader implements XMLEventReader {
		private Iterator<XMLEvent> events;
		XMLListEventReader(Iterable<XMLEvent> events) {
			this.events = events.iterator();
		}
		@Override public boolean hasNext() {
			return events.hasNext();
		}
		@Override public XMLEvent nextEvent() {
			return events.next();
		}
		@Override public void remove() {
			events.remove();
		}
		@Override public XMLEvent nextTag() throws XMLStreamException {
			for (XMLEvent e = nextEvent(); e != null; e = nextEvent()) {
				if (e.isStartElement() || e.isEndElement()) {
					return e;
				}
				else if (!isWhitespace(e)) {
					throw new IllegalStateException("Unexpected event: " + e);
				}
			}
			return null;
		}
		@Override public XMLEvent peek() throws XMLStreamException {
			throw new UnsupportedOperationException();
		}
		@Override public String getElementText() throws XMLStreamException {
			throw new UnsupportedOperationException();
		}
		@Override public Object getProperty(String name) throws IllegalArgumentException {
			throw new UnsupportedOperationException();
		}
		@Override public void close() throws XMLStreamException {
		}
		@Override public Object next() {
			return events.next();
		}
	}

	private void log(String s) {
		LOGGER.debug(s);
	}
}
