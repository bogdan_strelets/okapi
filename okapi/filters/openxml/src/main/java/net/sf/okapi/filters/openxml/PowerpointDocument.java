package net.sf.okapi.filters.openxml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;

import javax.xml.stream.XMLStreamException;

import net.sf.okapi.filters.openxml.ContentTypes.Types.Common;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Drawing;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Powerpoint;

public class PowerpointDocument extends DocumentType {
	private List<String> slideNames = new ArrayList<String>();
	private List<String> slideLayoutNames = new ArrayList<String>();

	private static final String SLIDE_LAYOUT_REL =
			Namespaces.DocumentRels.getDerivedURI("/slideLayout");

	public PowerpointDocument(OpenXMLZipFile zipFile,
			ConditionalParameters params) {
		super(zipFile, params);
	}

	@Override
	public ParseType getDefaultParseType() {
		return ParseType.MSPOWERPOINT;
	}

	@Override
	public void initialize() throws IOException, XMLStreamException {
		slideNames = findSlides();
		slideLayoutNames = findSlideLayouts(slideNames);
	}

	@Override
	public OpenXMLPartHandler getHandlerForFile(ZipEntry entry,
			String contentType, boolean squishable) {

		// Check to see if this is non-translatable
		if (!isTranslatableType(entry.getName(), contentType)) {
			return new NonTranslatablePartHandler(getZipFile(), entry);
		}

		OpenXMLContentFilter openXMLContentFilter =
				new OpenXMLContentFilter(getZipFile().getFactories(), getParams());
		openXMLContentFilter.setPartName(entry.getName());

		ParseType parseType = ParseType.MSPOWERPOINT;
		boolean shouldSquishPart = false;
		if (Powerpoint.SLIDE_TYPE.equals(contentType)) {
			openXMLContentFilter.setBInMainFile(true);
			openXMLContentFilter.setUpConfig(ParseType.MSPOWERPOINT);
			shouldSquishPart = squishable;
		}
		else if (contentType.equals(Common.CORE_PROPERTIES_TYPE)) {
			parseType = ParseType.MSWORDDOCPROPERTIES;
		}
		else if (contentType.equals(Powerpoint.COMMENTS_TYPE)) {
			parseType = ParseType.MSPOWERPOINTCOMMENTS;
		}
		else if (contentType.equals(Drawing.DIAGRAM_TYPE)) {
			shouldSquishPart = squishable;
		}
		openXMLContentFilter.setUpConfig(parseType);

		// Other configuration
		return new StandardPartHandler(openXMLContentFilter, getParams(), getZipFile(), entry, shouldSquishPart);
	}

	private boolean isTranslatableType(String entryName, String type) {
		if (!entryName.endsWith(".xml")) return false;
		if (type.equals(Powerpoint.SLIDE_TYPE)) return true;
		if (getParams().getTranslateDocProperties() && type.equals(Common.CORE_PROPERTIES_TYPE)) return true;
		if (getParams().getTranslateComments() && type.equals(Powerpoint.COMMENTS_TYPE)) return true;
		if (type.equals(Drawing.DIAGRAM_TYPE)) return true;
		if (getParams().getTranslatePowerpointNotes() && type.equals(Powerpoint.NOTES_TYPE)) return true;
		if (getParams().getTranslatePowerpointMasters()) {
			if (type.equals(Powerpoint.MASTERS_TYPE)) return true;
			// Layouts are translatable if we are translating masters and this particular layout is
			// in use by a slide
			if (type.equals(Powerpoint.LAYOUT_TYPE) && slideLayoutNames.contains(entryName)) return true;
		}
		return false;
	}

	/**
	 * Do additional reordering of the entries for PPTX files to make
	 * sure that slides are parsed in the correct order.  This is done
	 * by scraping information from one of the rels files and the 
	 * presentation itself in order to determine the proper order, rather
	 * than relying on the order in which things appeared in the zip.
	 * @throws XMLStreamException 
	 * @throws IOException 
	 */
	@Override
	public Enumeration<? extends ZipEntry> getZipFileEntries() throws IOException, XMLStreamException {
		OpenXMLZipFile zipFile = getZipFile();
		Enumeration<? extends ZipEntry> entries = zipFile.entries();
		List<? extends ZipEntry> entryList = Collections.list(entries);
		Collections.sort(entryList, new ZipEntryComparator(slideNames));
		return Collections.enumeration(entryList);
	}

	List<String> findSlides() throws IOException, XMLStreamException {
		OpenXMLZipFile zipFile = getZipFile();
		// XXX Not strictly correct, I should really look for the main document and then go from there
		Relationships rels = zipFile.getRelationships("ppt/_rels/presentation.xml.rels");
		Presentation pres = new Presentation(zipFile.getInputFactory(), rels);
		pres.parseFromXML(zipFile.getPartReader("ppt/presentation.xml"));
		return pres.getSlidePartNames();
	}

	/**
	 * Examine relationship information to find all layouts that are used in
	 * a slide in this document.  Return a list of their entry names, in order.
	 * @return list of entry names.
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	List<String> findSlideLayouts(List<String> slideNames) throws IOException, XMLStreamException {
		List<String> layouts = new ArrayList<String>();
		OpenXMLZipFile zipFile = getZipFile();
		for (String slideName : slideNames) {
			List<Relationships.Rel> rels =
					zipFile.getRelationshipsForPart(slideName).getRelByType(SLIDE_LAYOUT_REL);
			if (!rels.isEmpty()) {
				layouts.add(rels.get(0).target);
			}
		}
		return layouts;
	}
}
